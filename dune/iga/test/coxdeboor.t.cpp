/*
 * Copyright 2016 Maikel Nadolski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lest_cpp03.hpp"
#include <dune/iga/CoxDeBoor.hh>
#include <fstream>

lest::tests& specification()
{
  static lest::tests tests;
  return tests;
}

int main(int argc, char *argv[])
{
  return lest::run(specification(), argc, argv);
}


#define CASE( name ) lest_CASE( specification(), name )


CASE ("division through null is ok") {

  double x = 0;
  double y = 0;

  double result = x/y;
  EXPECT(std::isnan(result));
}

CASE ("CoxDeBoor does compile constructor and operator()") {

  Dune::IGA::CoxDeBoor<double> N({ 0, 1, 2, 3 });
  N(0.5, 0, 0);

}

CASE ("Order 0 works for uniform grids") {

  Dune::IGA::CoxDeBoor<double> N({ 0, 1, 2, 3 });
  double v = 0;
  double u = 0.5;
  double w = 1;

  for (std::size_t j = 0; j < 4; ++j)
    for (std::size_t i = 0; i < 3; ++i) {
      EXPECT(N(u+j, i, 0) == (i == j));
      EXPECT(N(v+j, i, 0) == (i == j));
      EXPECT(N(w+j, i, 0) == (i == (j+1)));
    }

  EXPECT_THROWS_AS(N(0, 3, 0), decltype(N)::KnotIsOutOfRange);
}

CASE ("make gnuplot output for order 1") {
  const std::size_t order = 1;
  Dune::IGA::CoxDeBoor<double> local_splines({0, 1, 2, 3});

  std::ofstream out("coxdeboor_order_1.dat");
  out << "# Cox de Boor algorithm to calculate base spline functions.\n";

  double x0 = 0;
  double xN = 3;
  double h = 0.01;

  for (std::size_t spline = 0; spline < 3-order; ++spline) {
    out << "#             X               Y\n";
    for (double x = x0; x < xN; x += h) {
      out << std::setw(15) << x << " ";
      out << std::setw(15) << local_splines(x, spline, order) << "\n";
    }
    out << "\n\n";
  }
}

CASE ("make gnuplot output for order 2") {
  const std::size_t order = 2;
  Dune::IGA::CoxDeBoor<double> local_splines({0,1,2,3});

  std::ofstream out("coxdeboor_order_2.dat");
  out << "# Cox de Boor algorithm to calculate base spline functions.\n";

  double x0 = 0;
  double xN = 3;
  double h = 0.01;

  out << "#             X               Y\n";
  for (double x = x0; x < xN; x += h) {
    out << std::setw(15) << x << " ";
    out << std::setw(15) << local_splines(x, 0, order) << "\n";
  }
}

CASE ("make the examples from http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/B-spline/bspline-ex-1.html ")
{
  std::vector<double> knots { 0, 0, 0, 0.3, 0.5, 0,5, 0,5 ,1 ,1, 1 };
  Dune::IGA::CoxDeBoor<double> splines(knots);

  std::ofstream out("coxdeboor_examples_mtu.edu.dat");
  std::vector<double> u(100);
  const std::size_t order = 1;
  double h = 0.01;
  double u0 = 0;
  std::generate(std::begin(u), std::end(u), [&] { return u0 = u0 + h; });
  for (std::size_t span = 0; span < knots.size()-order-1; ++span) {
    std::vector<double> spline = splines(u, span, order);
    for (std::size_t i = 0; i < u.size(); ++i)
      out << u[i] << " " << spline[i] << "\n";
    out << "\n\n";
  }
}
