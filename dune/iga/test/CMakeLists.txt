cmake_minimum_required(VERSION 2.8)

project(dune-iga-unittests)

include_directories(../../../)

set(CMAKE_CXX_FLAGS "-std=c++11 -Wall -Werror -Wfatal-errors -pedantic-errors")

add_executable(coxdeboor.t coxdeboor.t.cpp)
target_link_libraries(coxdeboor.t dunecommon)

add_test(NAME    coxdeboor
         COMMAND coxdeboor.t)
