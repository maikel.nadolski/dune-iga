/** \file gsl.hh
 * Define some things to support the guidelines proposed by isocpp.
 * Source: https://github.com/isocpp/CppCoreGuidelines
 *
 * This is taken from version from Microsoft's implementation and slightly
 * modified to fit our needs.
 * Source: https://github.com/Microsoft/GSL
 */

#ifndef DUNE_IGA_GSL_HH_
#define DUNE_IGA_GSL_HH_

#include <iterator>
#include <type_traits>

//
// There are three configuration options for this GSL implementation's behavior
// when pre/post conditions on the GSL types are violated:
//
// 1. GSL_TERMINATE_ON_CONTRACT_VIOLATION: std::terminate will be called (default)
// 2. GSL_THROW_ON_CONTRACT_VIOLATION: a gsl::fail_fast exception will be thrown
// 3. GSL_UNENFORCED_ON_CONTRACT_VIOLATION: nothing happens
//
#if !(defined(GSL_THROW_ON_CONTRACT_VIOLATION) ^ defined(GSL_TERMINATE_ON_CONTRACT_VIOLATION) ^ defined(GSL_UNENFORCED_ON_CONTRACT_VIOLATION))
#define GSL_THROW_ON_CONTRACT_VIOLATION
#endif


#define GSL_STRINGIFY_DETAIL(x) #x
#define GSL_STRINGIFY(x) GSL_STRINGIFY_DETAIL(x)


//
// GSL.assert: assertions
//

#if defined(GSL_THROW_ON_CONTRACT_VIOLATION)

#include <stdexcept>

namespace gsl
{
struct fail_fast : public std::runtime_error
{
  explicit fail_fast(char const* const message) : std::runtime_error(message) {}
};
}

#define Expects(cond)  if (!(cond)) \
  throw gsl::fail_fast("Precondition failure at " __FILE__ ": " GSL_STRINGIFY(__LINE__));
#define Ensures(cond)  if (!(cond)) \
  throw gsl::fail_fast("Postcondition failure at " __FILE__ ": " GSL_STRINGIFY(__LINE__));


#elif defined(GSL_TERMINATE_ON_CONTRACT_VIOLATION)

#include <exception>

#define Expects(cond)           if (!(cond)) std::terminate();
#define Ensures(cond)           if (!(cond)) std::terminate();


#elif defined(GSL_UNENFORCED_ON_CONTRACT_VIOLATION)

#define Expects(cond)
#define Ensures(cond)

#endif

namespace gsl {

// narrow_cast(): a searchable way to do narrowing casts of values
template<class T, class U>
inline constexpr T narrow_cast(U u) noexcept
{ return static_cast<T>(u); }

struct narrowing_error : public std::exception {};

namespace details
{
    template<class T, class U>
    struct is_same_signedness : public std::integral_constant<bool, std::is_signed<T>::value == std::is_signed<U>::value>
    {};
}

// narrow() : a checked version of narrow_cast() that throws if the cast changed the value
template<class T, class U>
inline T narrow(U u)
{
    T t = narrow_cast<T>(u);
    if (static_cast<U>(t) != u)
        throw narrowing_error();
    if (!details::is_same_signedness<T, U>::value && ((t < T{}) != (u < U{})))
        throw narrowing_error();
    return t;
}

} // namespace gsl


/*
 * Import gsl::narrow family into Dune's namespace
 */

namespace Dune { namespace IGA {

using ::gsl::narrow;
using ::gsl::narrow_cast;

}
}

#endif /* DUNE_IGA_GSL_HH_ */
