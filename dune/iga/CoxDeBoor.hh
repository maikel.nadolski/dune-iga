/*
 * Copyright 2016 Maikel Nadolski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef DUNE_IGA_COXDEBOOR_HH_
#define DUNE_IGA_COXDEBOOR_HH_

#include <dune/iga/gsl.hh>
#include <dune/iga/utility.hh>
#include <dune/common/exceptions.hh>

namespace Dune { namespace IGA {

template <class T>
  class CoxDeBoor {
    public:
      struct KnotIsOutOfRange: public Dune::Exception {};

      explicit CoxDeBoor(const std::vector<T>& knots) noexcept
      : knots_(knots), local_fn_(knots.size()-1)
      {
        std::sort(std::begin(knots_), std::end(knots_));
        Ensures(std::is_sorted(std::begin(knots_), std::end(knots_)));
      }

      std::vector<T> operator()(const std::vector<T>& us, std::size_t knot, std::size_t order)
      {
        std::vector<T> N(us.size());
        auto n = std::begin(N);
        for (T u : us)
          *n++ = (*this)(u, knot, order);
        return N;
      }

      double operator()(T u, std::size_t knot, std::size_t order)
      {
        if (!(knot+order+1 < knots_.size()))
          throw KnotIsOutOfRange();

        // get the range of knots relevant to calculate the coefficients for the given order
        std::size_t lower, upper;
        std::tie(lower, upper) = relevant_range(knot, order); // [lower, upper)
        std::size_t n = upper-lower;
        Expects(n <= order+1);
        Expects(lower < upper);

        // check if u is contained in the support of any local knot function
        // do a binary search to get the interval [u_i, u_{i+1}) containing `u` and
        // call that index i `local_knot`.
        if (!(knots_[lower] <= u && u < knots_[upper]))
          return 0;
        auto local_iter = std::upper_bound(std::begin(knots_), std::end(knots_), u) - 1;
        std::size_t local_knot = std::distance(std::begin(knots_), local_iter);
        Ensures(lower <= local_knot && local_knot < upper);

        // and set the initial values for order 0
        // it holds: N(u,i,p) = 1    for  u_i <= u < u_{i+1}
        //                    = 0    otherwise
        std::fill(std::begin(local_fn_)+lower, std::begin(local_fn_)+upper, 0);
        local_fn_[local_knot] = 1;

        // use Cox de Boor's formula to calculate N_ip(u) by
        // N(u,i+1,p) = (u-u_i)/(u_{i+p}-u_i) N(u,i,p-1) + (u_{i+1+p}-u)/(u_{i+1+p}-u_{i+1}) N(u,i+1,p-1)
        // Use bottom up loop instead of top down recursion.
        Expects(upper < knots_.size());
        for (std::size_t p = 0; p < order; ++p)
          for (std::size_t i=lower; i+p+1 < upper; ++i)
            local_fn_[i] = factor1(u,i,p+1)*local_fn_[i] + factor2(u,i,p+1)*local_fn_[i+1];

        return local_fn_[lower];
      }

    private:
      std::vector<T> knots_;
      std::vector<T> local_fn_;

      inline T factor1(T u, std::size_t i, std::size_t p) noexcept
      {
        Expects(i+p < knots_.size());
        if (almost_equal(knots_[i], knots_[i+p])) {
          return 0;
        }
        double factor = (u - knots_[i])/(knots_[i+p]-knots_[i]);
        Ensures(!std::isnan(factor));
        Ensures(!std::isinf(factor));
        return factor;
      }

      inline T factor2(T u, std::size_t i, std::size_t p) noexcept
      {
        Expects(i+p+1 < knots_.size());
        if (almost_equal(knots_[i+1], knots_[i+p+1]))
          return 0;
        double factor = (knots_[i+p+1] - u)/(knots_[i+p+1]-knots_[i+1]);
        Ensures(!std::isnan(factor));
        Ensures(!std::isinf(factor));
        return factor;
      }

      inline std::pair<std::size_t, std::size_t>
      relevant_range(std::size_t knot, std::size_t order) noexcept
      {
        Expects(knot+1 < knots_.size());
        std::size_t upper = std::min(knot+1+order, knots_.size()-1);
        Ensures(knot < upper);
        Ensures(upper <= knot+1+order);
        Ensures(upper < knots_.size());
        return { knot, upper };
      }
  };


} // namespace IGA
} // namespace Dune

#endif /* DUNE_IGA_COXDEBOOR_HH_ */
